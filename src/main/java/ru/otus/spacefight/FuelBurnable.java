package ru.otus.spacefight;

public interface FuelBurnable {
    int getFuelVolume();

    void setFuelVolume(int newValue);

    int getFuelBurnIntensity();
}
