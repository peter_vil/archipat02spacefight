package ru.otus.spacefight;

import java.util.Queue;

public class EnqueueRetryCommandForRetryExceptionHandler implements ExceptionHandler {
    private final Queue<Command> commandQueue;

    public EnqueueRetryCommandForRetryExceptionHandler(Queue<Command> commandQueue) {
        this.commandQueue = commandQueue;
    }

    @Override
    public void handle(Command command, Exception exception) {
        // Возможно, как-то прокомментируете этот код: не нравится, что обработчик исключения сам выбрасывает
        // исключение, и в то же время ReRetryCommand требует тип RetryCommand для "обертки"...
        if (! (command instanceof RetryCommand))
            throw new IllegalArgumentException(EnqueueRetryCommandForRetryExceptionHandler.class.getName() +
                    " accepts only command of class " + RetryCommand.class.getName());

        commandQueue.add(new ReRetryCommand((RetryCommand)command));
    }
}
