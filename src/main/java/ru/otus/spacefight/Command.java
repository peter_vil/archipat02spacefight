package ru.otus.spacefight;

public interface Command {
    void execute();
}
