package ru.otus.spacefight;

public interface EventLoopContinueStrategy {
    boolean stillContinue();
}
