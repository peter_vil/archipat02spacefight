package ru.otus.spacefight;

public class EventLoopNoLongerContinueStrategy implements EventLoopContinueStrategy {
    @Override
    public boolean stillContinue() {
        return false;
    }
}
