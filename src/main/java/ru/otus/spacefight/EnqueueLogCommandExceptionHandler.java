package ru.otus.spacefight;

import java.util.Queue;

public class EnqueueLogCommandExceptionHandler implements ExceptionHandler {
    private final Queue<Command> commandsQueue;

    private final Log log;

    public EnqueueLogCommandExceptionHandler(Queue<Command> commandsQueue, Log log) {
        this.commandsQueue = commandsQueue;
        this.log = log;
    }

    @Override
    public void handle(Command command, Exception exception) {
        commandsQueue.add(new LogCommand(log, command, exception));
    }
}
