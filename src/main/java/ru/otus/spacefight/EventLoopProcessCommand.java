package ru.otus.spacefight;

import ru.otus.spacefight.ioc.IoC;

import java.util.concurrent.BlockingQueue;

public class EventLoopProcessCommand implements Command {
    private final String loopName;

    public EventLoopProcessCommand(String loopName) {
        this.loopName = loopName;
    }

    @Override
    public void execute() {
        String eventLoopQueueIocKey = "EventLoopQueue." + loopName;
        String eventLoopStrategyIocKey = "EventLoopStrategy." + loopName;

        while (IoC.<EventLoopContinueStrategy>resolve(eventLoopStrategyIocKey).stillContinue()) {
            try {
                Command command = IoC.<BlockingQueue<Command>>resolve(eventLoopQueueIocKey).take();
                try {
                    command.execute();
                } catch (Exception e) {
                    try {
                        IoC.<ExceptionHandler>resolve("ExceptionHandler").handle(command, e);
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }
}
