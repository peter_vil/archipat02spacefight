package ru.otus.spacefight;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ExceptionRouterImpl implements ExceptionRouter {
    private final Map<RouteKey, ExceptionHandler> handlers = new HashMap<>();

    @Override
    public void routeExceptionHandling(Command command, Exception exception) {
        if (!invokeHandlerIfExists(new RouteKey(command.getClass(), exception.getClass()), command, exception))
            if (!invokeHandlerIfExists(new RouteKey(command.getClass(), null), command, exception))
                if (!invokeHandlerIfExists(new RouteKey(null, exception.getClass()), command, exception))
                    if (!invokeHandlerIfExists(new RouteKey(null, null), command, exception))
                        throw new RuntimeException(
                            String.format("Can't handle exception %s for command %s",
                                    exception.getClass().getName(),
                                    command.getClass().getName()),
                            exception);
    }

    private boolean invokeHandlerIfExists(RouteKey key, Command command, Exception exception) {
        ExceptionHandler handler = handlers.get(key);
        if (handler != null)
            handler.handle(command, exception);
        return handler != null;
    }

    public void registerHandler(Class<? extends Command> commandClass,
                                Class<? extends Exception> exceptionClass,
                                ExceptionHandler handler) {
        handlers.put(new RouteKey(commandClass, exceptionClass), handler);
    }

    private static class RouteKey {
        final Class<? extends Command> commandClass;

        final Class<? extends Exception> exceptionClass;

        public RouteKey(Class<? extends Command> commandClass, Class<? extends Exception> exceptionClass) {
            this.commandClass = commandClass;
            this.exceptionClass = exceptionClass;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            RouteKey routeKey = (RouteKey) o;
            return Objects.equals(commandClass, routeKey.commandClass)
                    && Objects.equals(exceptionClass, routeKey.exceptionClass);
        }

        @Override
        public int hashCode() {
            return Objects.hash(commandClass, exceptionClass);
        }
    }
}

