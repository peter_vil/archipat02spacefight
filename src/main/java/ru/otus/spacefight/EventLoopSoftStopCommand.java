package ru.otus.spacefight;

import ru.otus.spacefight.ioc.DependencyResolver;
import ru.otus.spacefight.ioc.IoC;

import java.util.concurrent.BlockingQueue;

public class EventLoopSoftStopCommand implements Command {
    private final String loopName;

    public EventLoopSoftStopCommand(String loopName) {
        this.loopName = loopName;
    }

    @Override
    public void execute() {
        BlockingQueue<Command> commandQueue = IoC.resolve("EventLoopQueue." + loopName);
        EventLoopContinueStrategy strategy = new EventLoopContinueWhileQueueNonEmptyStrategy(commandQueue);
        IoC.<Command>resolve("IoC.Register",
                        "EventLoopStrategy." + loopName,
                        (DependencyResolver) (o) -> strategy)
                .execute();
    }
}
