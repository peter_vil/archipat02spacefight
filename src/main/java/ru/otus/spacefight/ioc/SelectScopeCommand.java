package ru.otus.spacefight.ioc;

import ru.otus.spacefight.Command;

public class SelectScopeCommand implements Command {
    private final String scopeName;

    private final ThreadLocal<String> scopeNameHolder;

    private final Scopes scopes;

    public SelectScopeCommand(String scopeName, ThreadLocal<String> scopeNameHolder, Scopes scopes) {
        this.scopeName = scopeName;
        this.scopeNameHolder = scopeNameHolder;
        this.scopes = scopes;
    }

    @Override
    public void execute() {
        scopes.makeExisting(scopeName);

        scopeNameHolder.set(scopeName);
    }
}
