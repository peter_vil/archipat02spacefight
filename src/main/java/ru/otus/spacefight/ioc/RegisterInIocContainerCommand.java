package ru.otus.spacefight.ioc;

import ru.otus.spacefight.Command;

class RegisterInIocContainerCommand implements Command {
    private final DependenciesContainer iocContainer;

    private final String key;

    private final DependencyResolver resolver;

    public RegisterInIocContainerCommand(DependenciesContainer iocContainer,
                                         String key,
                                         DependencyResolver resolver) {
        this.iocContainer = iocContainer;
        this.key = key;
        this.resolver = resolver;
    }

    @Override
    public void execute() {
        iocContainer.register(key, resolver);
    }
}
