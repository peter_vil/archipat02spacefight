package ru.otus.spacefight.ioc;

@FunctionalInterface
public interface DependencyResolver {
    Object eval(Object[] args);
}
