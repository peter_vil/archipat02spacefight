package ru.otus.spacefight.ioc;

public class IocScopeException extends RuntimeException {
    public IocScopeException(String message) {
        super(message);
    }

    public IocScopeException(String message, Throwable cause) {
        super(message, cause);
    }

    public IocScopeException(Throwable cause) {
        super(cause);
    }
}
