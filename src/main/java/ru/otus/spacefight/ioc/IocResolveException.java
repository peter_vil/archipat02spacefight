package ru.otus.spacefight.ioc;

public class IocResolveException extends RuntimeException {
    public IocResolveException(String message) {
        super(message);
    }

    public IocResolveException(String message, Throwable cause) {
        super(message, cause);
    }

    public IocResolveException(Throwable cause) {
        super(cause);
    }
}
