package ru.otus.spacefight.ioc;

public class IoC {
    private final DependenciesContainer iocContainer;

    // По-хорошему, в конструктор передавать интерфейс DependenciesContainer, но откуда вообще
    // можно передать зависимость в контейнер зависимостей?
    private static final IoC instance = new IoC(new DependenciesContainerWithScopes());

    IoC(DependenciesContainer iocContainer) {
        this.iocContainer = iocContainer;

        iocContainer.register(
                "IoC.Register",
                args -> new RegisterInIocContainerCommand(
                        iocContainer,
                        (String)args[0],
                        (DependencyResolver)args[1]));
    }

    @SuppressWarnings("unchecked")
    <T> T resolve1(String key, Object ... args) {
        DependencyResolver resolver = iocContainer.get(key);
        if (resolver == null)
            throw new IocResolveException("Can't resolve '" + key + "'");

        return (T) resolver.eval(args);
    }

    public static <T> T resolve(String key, Object ... args) {
        return instance.resolve1(key, args);
    }
}

