package ru.otus.spacefight.ioc;

public interface DependenciesContainer {
    void register(String key, DependencyResolver resolver);

    DependencyResolver get(String key);
}
