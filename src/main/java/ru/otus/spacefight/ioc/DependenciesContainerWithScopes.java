package ru.otus.spacefight.ioc;

import java.util.HashMap;
import java.util.Map;

public class DependenciesContainerWithScopes implements DependenciesContainer, Scopes {
    private static final String DEFAULT_SCOPE = "DefaultScope";

    private final Map<String, Map<String, DependencyResolver>> resolvers = new HashMap<>();

    private final ThreadLocal<String> scopeNameHolder = ThreadLocal.withInitial(() -> DEFAULT_SCOPE);

    public DependenciesContainerWithScopes() {
        resolvers.put(DEFAULT_SCOPE, new HashMap<>());

        resolvers.get(DEFAULT_SCOPE).put(
                "Scope.Select",
                (args) -> new SelectScopeCommand((String) args[0], scopeNameHolder, this)
        );
    }

    @Override
    public void register(String key, DependencyResolver resolver) {
        getContainerForCurrentScopeOrThrow().put(key, resolver);
    }

    @Override
    public DependencyResolver get(String key) {
        DependencyResolver resolver = getContainerForCurrentScopeOrThrow().get(key);

        if (resolver == null)
            return resolvers.get(DEFAULT_SCOPE).get(key);
        else
            return resolver;
    }

    private Map<String, DependencyResolver> getContainerForCurrentScopeOrThrow() {
        Map<String, DependencyResolver> resolverForCurrentScope = resolvers.get(scopeNameHolder.get());
        if (resolverForCurrentScope == null)
            throw new IocScopeException("Scope '" + scopeNameHolder.get() + "' does not exist");

        return resolverForCurrentScope;
    }

    @Override
    public void makeExisting(String name) {
        resolvers.putIfAbsent(name, new HashMap<>());
    }
}
