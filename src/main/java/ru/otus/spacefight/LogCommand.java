package ru.otus.spacefight;

import java.util.HashMap;
import java.util.Map;

public class LogCommand implements Command {
    private final Log log;

    private final Command command;

    private final Exception exception;

    public LogCommand(Log log, Command command, Exception exception) {
        this.log = log;
        this.command = command;
        this.exception = exception;
    }

    @Override
    public void execute() {
        Map<String, Object> toLogging = new HashMap<>();
        toLogging.put("exception", exception);
        toLogging.put("command", command);

        log.log(toLogging);
    }
}
