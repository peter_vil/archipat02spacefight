package ru.otus.spacefight;

import java.util.Queue;

public class MoveContinuedWithFuelBurningCommand extends RepeatableCommand {

    public MoveContinuedWithFuelBurningCommand(Queue<Command> commandQueue, MoveWithFuelBurningCommand command) {
        super(commandQueue, command);
    }
}
