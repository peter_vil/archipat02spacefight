package ru.otus.spacefight;

public class MoveWithFuelBurningCommand implements Command {
    private final MacroCommand macro;

    public MoveWithFuelBurningCommand(Command fuelCheckCommand,
                                      Command moveCommand,
                                      Command fuelBurnCommand) {
        this.macro = new MacroCommand(
                new Command[] {
                        fuelCheckCommand,
                        moveCommand,
                        fuelBurnCommand
                });
    }

    @Override
    public void execute() {
        macro.execute();
    }
}
