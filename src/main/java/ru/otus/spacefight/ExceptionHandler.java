package ru.otus.spacefight;

public interface ExceptionHandler {
    void handle(Command command, Exception exception);
}
