package ru.otus.spacefight;

import java.util.Queue;

public class RepeatableCommand implements Command {
    private final Queue<Command> commandQueue;

    private final Command command;

    public RepeatableCommand(Queue<Command> commandQueue, Command command) {
        this.commandQueue = commandQueue;
        this.command = command;
    }

    @Override
    public void execute() {
        command.execute();

        commandQueue.add(this);
    }
}
