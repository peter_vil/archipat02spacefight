package ru.otus.spacefight;

import java.util.Map;

public interface Log {
    void log(Map<String, Object> what);
}
