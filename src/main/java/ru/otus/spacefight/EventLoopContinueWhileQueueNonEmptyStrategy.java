package ru.otus.spacefight;

import java.util.concurrent.BlockingQueue;

public class EventLoopContinueWhileQueueNonEmptyStrategy implements EventLoopContinueStrategy {
    private final BlockingQueue<Command> commandQueue;

    public EventLoopContinueWhileQueueNonEmptyStrategy(BlockingQueue<Command> commandQueue) {
        this.commandQueue = commandQueue;
    }

    @Override
    public boolean stillContinue() {
        return !commandQueue.isEmpty();
    }
}
