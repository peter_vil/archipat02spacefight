package ru.otus.spacefight;

public interface ExceptionRouter {
    void routeExceptionHandling(Command command, Exception exception);
}
