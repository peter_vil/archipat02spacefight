package ru.otus.spacefight;

public class RotateCommand implements Command {
    private final Rotatable rotatable;

    public RotateCommand(Rotatable rotatable) {
        this.rotatable = rotatable;
    }

    @Override
    public void execute() {
        rotatable.setDirectionNumber(
                (rotatable.getDirectionNumber() + rotatable.getAngleVelocity()) % rotatable.getDirectionsCount());
    }
}
