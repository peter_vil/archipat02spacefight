package ru.otus.spacefight;

public class RetryCommand implements Command {
    private final Command commandToRetry;

    public RetryCommand(Command commandToRetry) {
        this.commandToRetry = commandToRetry;
    }

    @Override
    public void execute() {
        commandToRetry.execute();
    }
}
