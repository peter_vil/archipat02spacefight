package ru.otus.spacefight;

public class FuelCheckCommand implements Command {
    private final FuelBurnable fuelBurnable;

    public FuelCheckCommand(FuelBurnable fuelBurnable) {
        this.fuelBurnable = fuelBurnable;
    }

    @Override
    public void execute() {
        if (fuelBurnable.getFuelVolume() < fuelBurnable.getFuelBurnIntensity())
            throw new CommandException();
    }
}
