package ru.otus.spacefight;

import ru.otus.spacefight.ioc.DependencyResolver;
import ru.otus.spacefight.ioc.IoC;

public class EventLoopHardStopCommand implements Command {
    private final String loopName;

    public EventLoopHardStopCommand(String loopName) {
        this.loopName = loopName;
    }

    @Override
    public void execute() {
        IoC.<Command>resolve("IoC.Register",
                        "EventLoopStrategy." + loopName,
                        (DependencyResolver) (o) -> new EventLoopNoLongerContinueStrategy())
                .execute();
    }
}
