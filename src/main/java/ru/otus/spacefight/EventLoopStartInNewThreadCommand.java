package ru.otus.spacefight;

import ru.otus.spacefight.ioc.DependencyResolver;
import ru.otus.spacefight.ioc.IoC;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

public class EventLoopStartInNewThreadCommand implements Command {
    private final String loopName;

    private final ExecutorService executorService;

    public EventLoopStartInNewThreadCommand(String loopName, ExecutorService executorService) {
        this.loopName = loopName;
        this.executorService = executorService;
    }

    @Override
    public void execute() {
        EventLoopContinueStrategy strategy = new EventLoopContinueInfiniteStrategy();
        IoC.<Command>resolve("IoC.Register",
                        "EventLoopStrategy." + loopName,
                        (DependencyResolver) (o) -> strategy)
                .execute();

        BlockingQueue<Command> queue = new LinkedBlockingQueue<>();
        IoC.<Command>resolve("IoC.Register",
                        "EventLoopQueue." + loopName,
                        (DependencyResolver) (o) -> queue)
                .execute();

        executorService
                .submit(() -> new EventLoopProcessCommand(loopName).execute());
    }
}
