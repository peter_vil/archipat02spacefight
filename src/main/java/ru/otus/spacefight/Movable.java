package ru.otus.spacefight;

public interface Movable {
    Coordinates getPosition();

    void setPosition(Coordinates coordinates);

    Coordinates getVelocity();
}
