package ru.otus.spacefight;

public class FuelBurnCommand implements Command {
    private final FuelBurnable fuelBurnable;

    public FuelBurnCommand(FuelBurnable fuelBurnable) {
        this.fuelBurnable = fuelBurnable;
    }

    @Override
    public void execute() {
        fuelBurnable.setFuelVolume(
                fuelBurnable.getFuelVolume() - fuelBurnable.getFuelBurnIntensity()
        );
    }
}
