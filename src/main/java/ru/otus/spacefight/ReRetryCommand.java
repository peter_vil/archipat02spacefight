package ru.otus.spacefight;

public class ReRetryCommand implements Command {
    private final RetryCommand retryCommand;

    public ReRetryCommand(RetryCommand retryCommand) {
        this.retryCommand = retryCommand;
    }

    @Override
    public void execute() {
        retryCommand.execute();
    }
}
