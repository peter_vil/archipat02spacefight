package ru.otus.spacefight;

public class MoveCommand implements Command {
    private final Movable movable;

    public MoveCommand(Movable movable) {
        this.movable = movable;
    }

    @Override
    public void execute() {
        movable.setPosition(
                Coordinates.plus(
                        movable.getPosition(),
                        movable.getVelocity()
                )
        );
    }
}
