package ru.otus.spacefight;

import java.util.Queue;

public class ExceptionRoutingStrategy1 {
    private final ExceptionRouterImpl exceptionRouter = new ExceptionRouterImpl();

    public ExceptionRoutingStrategy1(Queue<Command> commandQueue, Log log) {
        this.exceptionRouter.registerHandler(
                null,
                null,
                new EnqueueCommandForRetryExceptionHandler(commandQueue));

        this.exceptionRouter.registerHandler(
                RetryCommand.class,
                null,
                new EnqueueLogCommandExceptionHandler(commandQueue, log));
    }

    public ExceptionRouter get() {
        return exceptionRouter;
    }
}
