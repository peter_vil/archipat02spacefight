package ru.otus.spacefight;

import java.util.Queue;

public class ExceptionRoutingStrategy2 {
    private final ExceptionRouterImpl exceptionRouter = new ExceptionRouterImpl();

    public ExceptionRoutingStrategy2(Queue<Command> commandQueue, Log log) {
        this.exceptionRouter.registerHandler(
                null,
                null,
                new EnqueueCommandForRetryExceptionHandler(commandQueue));

        this.exceptionRouter.registerHandler(
                RetryCommand.class,
                null,
                new EnqueueRetryCommandForRetryExceptionHandler(commandQueue));

        this.exceptionRouter.registerHandler(
                ReRetryCommand.class,
                null,
                new EnqueueLogCommandExceptionHandler(commandQueue, log));
    }

    public ExceptionRouter get() {
        return exceptionRouter;
    }

}
