package ru.otus.spacefight;

import java.util.Queue;

public class EnqueueCommandForRetryExceptionHandler implements ExceptionHandler {
    private final Queue<Command> commandQueue;

    public EnqueueCommandForRetryExceptionHandler(Queue<Command> commandQueue) {
        this.commandQueue = commandQueue;
    }

    @Override
    public void handle(Command command, Exception exception) {
        commandQueue.add(new RetryCommand(command));
    }
}
