package ru.otus.spacefight.adapters;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DependenciesForAdaptersPluginHelper {
    private static final Pattern methodPattern = Pattern.compile("^(get|set)([A-Z]\\w+)$");

    DependenciesForAdaptersPluginHelper help(Class<?> clazz, OutputStream outputStream) throws IOException {
        for (Method method : clazz.getDeclaredMethods()) {
            Matcher match = methodPattern.matcher(method.getName());
            if (!match.matches())
                outputStream.write(("// Warning! On interface " + clazz.getName() + " found not getter or setter method: " + method.getName() + "\n").getBytes());
            else
                outputStream.write(
                        String.format(
                                "IoC.<Command>resolve(\"IoC.Register\", \"Operations.%s:%s.%s\", (DependencyResolver) o -> { throw new UnsupportedOperationException(); }).execute()\n",
                                clazz.getSimpleName(),
                                match.group(2).toLowerCase(),
                                match.group(1)).getBytes());
        }
        return this;
    }
}
