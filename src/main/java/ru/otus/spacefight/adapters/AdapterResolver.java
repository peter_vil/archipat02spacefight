package ru.otus.spacefight.adapters;

import ru.otus.spacefight.UObject;
import ru.otus.spacefight.ioc.DependencyResolver;

import java.util.HashMap;
import java.util.Map;

public class AdapterResolver implements DependencyResolver {
    private final Map<Class<?>, AdapterGenerator<?>> cachedAdapterGenerators = new HashMap<>();

    @Override
    public Object eval(Object[] args) {
        Class<?> clazz = (Class<?>) args[0];
        return cachedAdapterGenerators
                .computeIfAbsent(clazz, AdapterGenerator::new)
                .wrapUObject((UObject)args[1]);
    }
}
