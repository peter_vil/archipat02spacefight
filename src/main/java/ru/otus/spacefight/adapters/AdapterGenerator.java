package ru.otus.spacefight.adapters;

import ru.otus.spacefight.UObject;
import ru.otus.spacefight.ioc.IoC;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AdapterGenerator<T> {
    private static final Pattern methodPattern = Pattern.compile("^(get|set)([A-Z]\\w+)$");

    private final Map<Method, BiFunction<UObject, Object, Object>> methodImplementations = new HashMap<>();

    private final Class<T> clazz;

    public AdapterGenerator(Class<T> clazz) {
        this.clazz = clazz;

        createMethodImplementationsViaIoC();
    }

    private void createMethodImplementationsViaIoC() {
        for (Method method : clazz.getDeclaredMethods()) {
            Matcher match = methodPattern.matcher(method.getName());
            if (!match.matches())
                throw new AdapterException("Adapter supports only getX or setX interface methods, but have '" +
                        method.getName() + "' on interface " + clazz.getName());

            if ("set".equals(match.group(1))) {
                if (method.getParameters().length != 1)
                    throw new AdapterException("Interface " + clazz.getName() + " has 'set' method with not a single parameter");

                methodImplementations.put(
                        method,
                        (o1, o2) -> IoC.resolve(
                                String.format("Operations.%s:%s.set",
                                        clazz.getSimpleName(),
                                        match.group(2).toLowerCase()),
                                o1, o2));
            } else {
                if (method.getParameters().length != 0)
                    throw new AdapterException("Interface " + clazz.getName() + " has 'get' method with parameters");

                methodImplementations.put(
                        method,
                        (o1, o2) -> IoC.resolve(
                                String.format("Operations.%s:%s.get",
                                        clazz.getSimpleName(),
                                        match.group(2).toLowerCase()),
                                o1));
            }
        }
    }

    @SuppressWarnings("unchecked")
    public T wrapUObject(UObject uObject)  {
        return (T) Proxy.newProxyInstance(
                AdapterGenerator.class.getClassLoader(),
                new Class[]{clazz},
                new InvocationHandlerViaIoC(uObject)
        );
    }

    private class InvocationHandlerViaIoC implements InvocationHandler {
        private final UObject uObject;

        public InvocationHandlerViaIoC(UObject uObject) {
            this.uObject = uObject;
        }

        @Override
        public Object invoke(Object o, Method method, Object[] objects) {
            BiFunction<UObject, Object, Object> impl =  methodImplementations.get(method);
            if (impl == null)
                throw new AdapterException("No implementation for '" + method.getName() + "' on adapter for interface " + clazz.getName());
            return impl.apply(uObject, objects == null ? null : objects[0]);
        }
    }
}
