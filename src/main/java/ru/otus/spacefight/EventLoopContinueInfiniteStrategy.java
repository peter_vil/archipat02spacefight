package ru.otus.spacefight;

public class EventLoopContinueInfiniteStrategy implements EventLoopContinueStrategy {
    @Override
    public boolean stillContinue() {
        return !Thread.currentThread().isInterrupted();
    }
}
