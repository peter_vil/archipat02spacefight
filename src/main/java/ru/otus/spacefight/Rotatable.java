package ru.otus.spacefight;

public interface Rotatable {

    int getDirectionNumber();

    void setDirectionNumber(int n);

    int getAngleVelocity();

    int getDirectionsCount();

}
