package ru.otus.spacefight;

import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class ReRetryCommandTest {
    @Test
    public void whenReRetryCommandExecuted_thenExecutedOriginalCommand () {
        Command commandToRetry = mock(Command.class);

        new ReRetryCommand(new RetryCommand(commandToRetry)).execute();

        verify(commandToRetry).execute();
    }

}