package ru.otus.spacefight.ioc;

import org.junit.jupiter.api.Test;
import ru.otus.spacefight.Command;
import ru.otus.spacefight.ioc.DependencyResolver;
import ru.otus.spacefight.ioc.IoC;
import ru.otus.spacefight.ioc.IocResolveException;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class IoCTest {
    @Test
    public void whenIocResolve_thenResolveStrategyCalled() {
        DependencyResolver resolveStrategy = mock(DependencyResolver.class);
        IoC.<Command>resolve("IoC.Register",
                        "ExistingKey",
                        resolveStrategy)
                .execute();

        Object[] args = new Object[] { "Arg1", "Arg2" };
        IoC.resolve("ExistingKey", args);

        verify(resolveStrategy).eval(args);
    }


    @Test
    public void whenIocDosNotResolve_thenThrow() {
        assertThatThrownBy(() -> IoC.resolve("ExistingKey"))
                .isInstanceOf(IocResolveException.class);
    }

}