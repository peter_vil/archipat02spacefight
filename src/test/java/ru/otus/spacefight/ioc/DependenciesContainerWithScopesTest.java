package ru.otus.spacefight.ioc;

import org.junit.jupiter.api.Test;
import ru.otus.spacefight.Command;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Т.к. скоуп зависим от текущего потока, каждый тест выполняем в своем потоке чтобы
 * тесты были "чистыми" и не зависели от возможно оставшегося с другого теста значения в ThreadLocal
 */
class DependenciesContainerWithScopesTest {
    @Test
    public void whenNoDependencyInSelectedScopeScope_thenResolveFromDefaultScope() throws InterruptedException, ExecutionException {
        Object objectInDefaultScope = new Object();
        IoC ioc = new IoC(new DependenciesContainerWithScopes());

        Future<Object> future = Executors.newSingleThreadExecutor().submit(() -> {
            ioc.<Command>resolve1("IoC.Register", "DepInDefaultScope",
                    (DependencyResolver) (args) -> objectInDefaultScope).execute();
            ioc.<Command>resolve1("Scope.Select", "NonDefaultScope").execute();

            return ioc.resolve1("DepInDefaultScope");
        });

        assertThat(future.get()).isEqualTo(objectInDefaultScope);
    }

    @Test
    public void whenDependenciesRegisteredInDifferentScopes_thenResolveDifferentObjectsInDifferentScopes() throws InterruptedException, ExecutionException {
        Object objectInScope1 = new Object();
        Object objectInScope2 = new Object();
        IoC ioc = new IoC(new DependenciesContainerWithScopes());

        ExecutorService executor = Executors.newFixedThreadPool(2);

        Future<Object> futureScope1 = executor.submit(() -> {
            ioc.<Command>resolve1("Scope.Select", "Scope1").execute();
            ioc.<Command>resolve1("IoC.Register", "Dependency",
                    (DependencyResolver) (args) -> objectInScope1).execute();

            return ioc.resolve1("Dependency");
        });

        Future<Object> futureScope2 = executor.submit(() -> {
            ioc.<Command>resolve1("Scope.Select", "Scope2").execute();
            ioc.<Command>resolve1("IoC.Register", "Dependency",
                    (DependencyResolver) (args) -> objectInScope2).execute();

            return ioc.resolve1("Dependency");
        });

        assertThat(futureScope1.get()).isEqualTo(objectInScope1);
        assertThat(futureScope2.get()).isEqualTo(objectInScope2);
    }

    @Test
    public void whenDependenciesRegisteredInDifferentScopes_thenResolveDifferentObjectsAfterChangeScope() throws InterruptedException, ExecutionException {
        Object objectInScope1 = new Object();
        Object objectInScope2 = new Object();
        IoC ioc = new IoC(new DependenciesContainerWithScopes());

        Future<Void> future = Executors.newSingleThreadExecutor().submit(() -> {
            ioc.<Command>resolve1("Scope.Select", "Scope1").execute();
            ioc.<Command>resolve1("IoC.Register", "Dependency",
                    (DependencyResolver) (args) -> objectInScope1).execute();

            ioc.<Command>resolve1("Scope.Select", "Scope2").execute();
            ioc.<Command>resolve1("IoC.Register", "Dependency",
                    (DependencyResolver) (args) -> objectInScope2).execute();

            ioc.<Command>resolve1("Scope.Select", "Scope1").execute();
            assertThat(ioc.<Object>resolve1("Dependency")).isEqualTo(objectInScope1);

            ioc.<Command>resolve1("Scope.Select", "Scope2").execute();
            assertThat(ioc.<Object>resolve1("Dependency")).isEqualTo(objectInScope2);

            return null;
        });
        future.get();
    }

}