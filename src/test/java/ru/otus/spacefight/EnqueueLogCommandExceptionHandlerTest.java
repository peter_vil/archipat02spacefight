package ru.otus.spacefight;

import org.junit.jupiter.api.Test;

import java.util.LinkedList;
import java.util.Queue;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.mock;

class EnqueueLogCommandExceptionHandlerTest {
    @Test
    public void whenHandleException_thenEnqueueLogCommand() {
        Queue<Command> commandsQueue = new LinkedList<>();
        Log log = mock(Log.class);
        Command command = mock(Command.class);

        new EnqueueLogCommandExceptionHandler(commandsQueue, log).handle(command, new RuntimeException());

        assertThat(commandsQueue.poll()).isInstanceOf(LogCommand.class);
    }

}