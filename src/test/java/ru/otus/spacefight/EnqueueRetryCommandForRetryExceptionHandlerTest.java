package ru.otus.spacefight;

import org.junit.jupiter.api.Test;

import java.util.LinkedList;
import java.util.Queue;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.Mockito.mock;

class EnqueueRetryCommandForRetryExceptionHandlerTest {
    @Test
    public void whenHandleException_thenEnqueueRetryCommand() {
        Queue<Command> commandsQueue = new LinkedList<>();
        Command command = mock(Command.class);
        Command retryCommand = new RetryCommand(command);

        new EnqueueRetryCommandForRetryExceptionHandler(commandsQueue).handle(retryCommand, new RuntimeException());

        assertThat(commandsQueue.poll()).isInstanceOf(ReRetryCommand.class);
    }

    @Test
    public void whenHandleExceptionWithCommandWhatIsNotRetry_thenThrow() {
        Queue<Command> commandsQueue = new LinkedList<>();
        Command command = mock(Command.class);

        assertThatThrownBy(() ->
                new EnqueueRetryCommandForRetryExceptionHandler(commandsQueue)
                        .handle(command, new RuntimeException())
        );
    }

}