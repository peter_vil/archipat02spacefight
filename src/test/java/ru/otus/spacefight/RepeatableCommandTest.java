package ru.otus.spacefight;

import org.junit.jupiter.api.Test;

import java.util.LinkedList;
import java.util.Queue;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

class RepeatableCommandTest {
    @Test
    public void whenCommandIsRepeatable_thenExecuteUsefulCommandAndRepeatIt() {
        Queue<Command> commandQueue = new LinkedList<>();
        Command usefulCommand = mock(Command.class);

        commandQueue.add(new RepeatableCommand(commandQueue, usefulCommand));

        int iterationCount = 5;
        int iteration = 0;

        do {
            Command nextCommand = commandQueue.poll();
            assertThat(nextCommand).isNotNull();

            nextCommand.execute();
        } while (++iteration < iterationCount);

        verify(usefulCommand, times(iterationCount)).execute();
    }

}