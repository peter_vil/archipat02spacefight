package ru.otus.spacefight;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import ru.otus.spacefight.ioc.DependencyResolver;
import ru.otus.spacefight.ioc.IoC;

import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class EventLoopTest {
    private static class MutableWrapper<T> {
        private volatile T value;

        MutableWrapper(T value) {
            this.value = value;
        }

        void set(T newValue) {
            value = newValue;
        }

        T get() {
            return value;
        }
    }

    @FunctionalInterface
    private interface ThrowWrapper {
        void execute() throws Exception;
    }

    private static class ThrowableExceptionCommand implements Command {
        private final ThrowWrapper delegate;

        public ThrowableExceptionCommand(ThrowWrapper delegate) {
            this.delegate = delegate;
        }

        @Override
        public void execute() {
            try {
                delegate.execute();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    @BeforeAll
    static void registerDependencies() {
        ExceptionHandler exceptionHandler = (c, e) -> e.printStackTrace();
        IoC.<Command>resolve("IoC.Register", "ExceptionHandler",
                (DependencyResolver) (o) -> exceptionHandler).execute();
    }

    @Test
    public void whenStartCommandExecuted_thenRunInOtherThread() throws InterruptedException {
        String gameName = "game-" + UUID.randomUUID();
        CountDownLatch cdl = new CountDownLatch(1);
        MutableWrapper<Thread> thread = new MutableWrapper<>(null);

        new EventLoopStartInNewThreadCommand(gameName, Executors.newSingleThreadExecutor()).execute();
        Queue<Command> queue = IoC.resolve("EventLoopQueue." + gameName);
        queue.add(() -> thread.set(Thread.currentThread()));
        queue.add(cdl::countDown);
        queue.add(() -> Thread.currentThread().interrupt());

        assertThat(cdl.await(5, TimeUnit.SECONDS)).isTrue();
        assertThat(thread.get()).isNotEqualTo(Thread.currentThread());
    }

    @Test
    public void whenSoftStopCommandExecuted_thenAllCommandsInQueueExecutedAndStop() throws InterruptedException {
        String gameName = "game-" + UUID.randomUUID();
        int cmdCount = 5;
        CountDownLatch cdl = new CountDownLatch(cmdCount);
        Semaphore mutex = new Semaphore(1);
        ExecutorService executorService = Executors.newSingleThreadExecutor();

        new EventLoopStartInNewThreadCommand(gameName, executorService).execute();
        executorService.shutdown();

        Queue<Command> queue = IoC.resolve("EventLoopQueue." + gameName);
        // Нужно притормозить поток event-loop пока не подготовим очередь в потоке теста
        // Очередь должна гарантировано содержать команды после SoftStop
        queue.add(new ThrowableExceptionCommand(mutex::acquire));
        queue.add(new EventLoopSoftStopCommand(gameName));
        for (int i = 0; i < cmdCount; i++)
            queue.add(cdl::countDown);
        mutex.release(); // Очередь заполнена как надо для теста, снимаем блокировку event-loop

        assertThat(cdl.await(5, TimeUnit.SECONDS)).isTrue();
        assertThat(executorService.awaitTermination(5, TimeUnit.SECONDS)).isTrue();
    }

    @Test
    public void whenHardStopCommandExecuted_thenNoMoreCommandsExecuted() throws InterruptedException {
        String gameName = "game-" + UUID.randomUUID();
        int cmdCount = 5;
        AtomicInteger cnt = new AtomicInteger();
        Semaphore mutex = new Semaphore(1);
        ExecutorService executorService = Executors.newSingleThreadExecutor();

        new EventLoopStartInNewThreadCommand(gameName, executorService).execute();
        executorService.shutdown();

        Queue<Command> queue = IoC.resolve("EventLoopQueue." + gameName);
        queue.add(new ThrowableExceptionCommand(mutex::acquire));
        queue.add(new EventLoopHardStopCommand(gameName));
        for (int i = 0; i < cmdCount; i++)
            queue.add(cnt::incrementAndGet);
        mutex.release();

        assertThat(executorService.awaitTermination(5, TimeUnit.SECONDS)).isTrue();
        assertThat(cnt.get()).isEqualTo(0);
    }
}