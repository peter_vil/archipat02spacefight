package ru.otus.spacefight;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.*;

class LogCommandTest {
    @Test
    public void whenLogCommandExecuted_thenLogIt () {
        Log log = mock(Log.class);
        Command commandBringException = mock(Command.class);
        Exception exception = new Exception();

        new LogCommand(log, commandBringException, exception).execute();

        Map<String, Object> expected = new HashMap<>();
        expected.put("exception", exception);
        expected.put("command", commandBringException);
        verify(log).log(expected);
    }
}