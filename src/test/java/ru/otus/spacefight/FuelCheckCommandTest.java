package ru.otus.spacefight;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.Mockito.*;

class FuelCheckCommandTest {
    @Test
    public void whenCheckExecutedAndFuelEnough_thenNoException() {
        FuelBurnable fuelBurnable = mock(FuelBurnable.class);
        when(fuelBurnable.getFuelVolume()).thenReturn(1000);
        when(fuelBurnable.getFuelBurnIntensity()).thenReturn(10);

        new FuelCheckCommand(fuelBurnable).execute();
    }

    @Test
    public void whenCheckExecutedAndFuelInsufficiently_thenException() {
        FuelBurnable fuelBurnable = mock(FuelBurnable.class);
        when(fuelBurnable.getFuelVolume()).thenReturn(9);
        when(fuelBurnable.getFuelBurnIntensity()).thenReturn(10);

        assertThatThrownBy(() ->
                new FuelCheckCommand(fuelBurnable).execute()
        ).isInstanceOf(CommandException.class);
    }

}