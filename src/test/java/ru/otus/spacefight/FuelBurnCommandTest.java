package ru.otus.spacefight;

import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

class FuelBurnCommandTest {
    @Test
    public void whenBurnCommandExecuted_thenSetNewFuelVolume() {
        FuelBurnable fuelBurnable = mock(FuelBurnable.class);
        when(fuelBurnable.getFuelVolume()).thenReturn(1000);
        when(fuelBurnable.getFuelBurnIntensity()).thenReturn(10);

        new FuelBurnCommand(fuelBurnable).execute();

        verify(fuelBurnable).setFuelVolume(990);
    }

}