package ru.otus.spacefight;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.Mockito.*;

class MacroCommandTest {
    @Test
    public void whenMacroCommandExecuted_thenEveryInternalCommandExecuted() {
        Command[] commands = new Command[5];
        for (int i =0; i < commands.length; i++)
            commands[i] = mock(Command.class);

        new MacroCommand(commands).execute();

        for (Command command : commands)
            verify(command).execute();
    }

    @Test
    public void whenAnyInternalMacroCommandThrows_thenMacroCommandStopsAndThrows() {
        Command command1 = mock(Command.class);
        Command command2 = mock(Command.class);
        doThrow(new CommandException()).when(command1).execute();

        assertThatThrownBy(() ->
                new MacroCommand(new Command[] {command1, command2}).execute()
        );

        verify(command2, never()).execute();
    }
}