package ru.otus.spacefight.adapters;

import org.junit.jupiter.api.Test;
import ru.otus.spacefight.Command;
import ru.otus.spacefight.UObject;
import ru.otus.spacefight.ioc.DependencyResolver;
import ru.otus.spacefight.ioc.IoC;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class AdapterGeneratorTest {
    private interface TestInterfaceWithProperties {
        String getTestProperty1();

        void setTestProperty1(String newValue);
    }

    @Test
    public void whenGetPropertyFromAdapter_thenResolvePropertyFromIoc() {
        AdapterGenerator<TestInterfaceWithProperties> adapterGenerator = new AdapterGenerator<>(TestInterfaceWithProperties.class);
        TestInterfaceWithProperties adapter = adapterGenerator.wrapUObject(new UObject());

        IoC.<Command>resolve(
                        "IoC.Register",
                        "Operations.TestInterfaceWithProperties:testproperty1.get",
                        (DependencyResolver) o -> "value1")
                .execute();

        assertThat(adapter.getTestProperty1()).isEqualTo("value1");
    }

    @Test
    public void whenSetPropertyFromAdapter_thenResolvePropertySetterFromIocAndCallIt() {
        UObject uObject = new UObject();
        AdapterGenerator<TestInterfaceWithProperties> adapterGenerator = new AdapterGenerator<>(TestInterfaceWithProperties.class);
        TestInterfaceWithProperties adapter = adapterGenerator.wrapUObject(uObject);

        DependencyResolver setter = mock();
        IoC.<Command>resolve(
                        "IoC.Register",
                        "Operations.TestInterfaceWithProperties:testproperty1.set",
                        setter)
                .execute();

        adapter.setTestProperty1("value2");
        verify(setter).eval(new Object[]{uObject, "value2"});
    }

}