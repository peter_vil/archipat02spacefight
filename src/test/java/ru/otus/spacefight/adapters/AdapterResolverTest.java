package ru.otus.spacefight.adapters;

import org.junit.jupiter.api.Test;
import ru.otus.spacefight.Command;
import ru.otus.spacefight.UObject;
import ru.otus.spacefight.ioc.IoC;

class AdapterResolverTest {
    private interface TestInterfaceForAdapt {
    }

    @Test
    public void whenRegisteredAdapterResolve_thenSuccessfullyResolveAdapter() {
        IoC.<Command>resolve("IoC.Register", "Adapter", new AdapterResolver()).execute();

        IoC.resolve("Adapter", TestInterfaceForAdapt.class, new UObject());
    }
}