package ru.otus.spacefight.adapters;

import org.junit.jupiter.api.Test;
import ru.otus.spacefight.FuelBurnable;
import ru.otus.spacefight.Movable;
import ru.otus.spacefight.Rotatable;

import java.io.IOException;

class DependenciesForAdaptersPluginHelperTest {
    @Test
    public void generateSkeletonToStdout() throws IOException {
        new DependenciesForAdaptersPluginHelper()
                .help(Movable.class, System.out)
                .help(Rotatable.class, System.out)
                .help(FuelBurnable.class, System.out);
    }

}