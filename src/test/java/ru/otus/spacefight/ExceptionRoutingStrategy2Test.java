package ru.otus.spacefight;

import org.junit.jupiter.api.Test;

import java.util.LinkedList;
import java.util.Queue;

import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.Mockito.*;

class ExceptionRoutingStrategy2Test {
    @Test
    public void whenCommandThrowsException_thenFirstRetryAndSecondRetryAndThirdLog() {
        Log log = mock(Log.class);
        Queue<Command> commandQueue = new LinkedList<>();
        Command commandThrowsException = mock(Command.class);
        Exception exception = new RuntimeException();
        doThrow(exception).when(commandThrowsException).execute();

        ExceptionRouter router = new ExceptionRoutingStrategy2(commandQueue, log).get();
        commandQueue.add(commandThrowsException);

        while (!commandQueue.isEmpty()) {
            Command command = commandQueue.poll();
            try {
                command.execute();
            } catch (Exception e) {
                router.routeExceptionHandling(command, e);
            }
        }

        verify(commandThrowsException, times(3)).execute();
        verify(log).log(anyMap());
    }

}