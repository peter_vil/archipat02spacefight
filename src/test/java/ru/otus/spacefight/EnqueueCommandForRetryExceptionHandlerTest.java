package ru.otus.spacefight;

import org.junit.jupiter.api.Test;

import java.util.LinkedList;
import java.util.Queue;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.mock;

class EnqueueCommandForRetryExceptionHandlerTest {
    @Test
    public void whenHandleException_thenEnqueueRetryCommand() {
        Queue<Command> commandsQueue = new LinkedList<>();
        Command command = mock(Command.class);

        new EnqueueCommandForRetryExceptionHandler(commandsQueue).handle(command, new RuntimeException());

        assertThat(commandsQueue.poll()).isInstanceOf(RetryCommand.class);
    }
}