package ru.otus.spacefight;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.Mockito.*;

class MoveCommandTest {
    //Для объекта, находящегося в точке (12, 5) и движущегося со скоростью (-7, 3) движение меняет положение объекта на (5, 8)
    @Test
    public void whenMovableInPointAndHasVelocity_thenSetNewCoordinates () {
        Movable movable = mock(Movable.class);
        when(movable.getPosition()).thenReturn(new Coordinates(12, 5));
        when(movable.getVelocity()).thenReturn(new Coordinates(-7, 3));

        new MoveCommand(movable).execute();

        verify(movable).setPosition(new Coordinates(5, 8));
    }

    //Попытка сдвинуть объект, у которого невозможно прочитать положение в пространстве, приводит к ошибке
    @Test
    public void whenCanNotGetCurrentPosition_thenThrow() {
        Movable movable = mock(Movable.class);
        when(movable.getVelocity()).thenReturn(new Coordinates(-7, 3));
        doThrow(new RuntimeException()).when(movable).getPosition();

        assertThatThrownBy(() -> new MoveCommand(movable).execute());
    }

    //Попытка сдвинуть объект, у которого невозможно прочитать значение мгновенной скорости, приводит к ошибке
    @Test
    public void whenCanNotGetVelocity_thenThrow() {
        Movable movable = mock(Movable.class);
        when(movable.getPosition()).thenReturn(new Coordinates(12, 5));
        doThrow(new RuntimeException()).when(movable).getVelocity();

        assertThatThrownBy(() -> new MoveCommand(movable).execute());
    }

    //Попытка сдвинуть объект, у которого невозможно изменить положение в пространстве, приводит к ошибке
    @Test
    public void whenCanNotSetNewPosition_thenThrow() {
        Movable movable = mock(Movable.class);
        when(movable.getPosition()).thenReturn(new Coordinates(12, 5));
        when(movable.getVelocity()).thenReturn(new Coordinates(-7, 3));
        doThrow(new RuntimeException()).when(movable).setPosition(any());

        assertThatThrownBy(() -> new MoveCommand(movable).execute());
    }

}