package ru.otus.spacefight;

import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

class RotateCommandTest {
    //При исходном направлении номер 8 из 10 возможных и угловой скорости равной 3 вращение устанавливает направление 1
    @Test
    public void whenRotateOverZeroAxis_thenSetNewDirectionAsExpected() {
        Rotatable rotatable = mock(Rotatable.class);
        when(rotatable.getDirectionNumber()).thenReturn(8);
        when(rotatable.getDirectionsCount()).thenReturn(10);
        when(rotatable.getAngleVelocity()).thenReturn(3);

        new RotateCommand(rotatable).execute();

        verify(rotatable).setDirectionNumber(1);
    }

}