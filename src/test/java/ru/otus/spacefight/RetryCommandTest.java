package ru.otus.spacefight;

import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class RetryCommandTest {
    @Test
    public void whenRetryCommandExecuted_thenExecuteOriginalCommand() {
        Command commandToRetry = mock(Command.class);

        new RetryCommand(commandToRetry).execute();

        verify(commandToRetry).execute();
    }

}